﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LoginForm_Database_.Models;

namespace LoginForm_Database_.Controllers
{
    public class RagistrationsController : Controller
    {
        private MVC_Demo1Entities db = new MVC_Demo1Entities();

        // GET: Ragistrations
        public ActionResult Index()
        {
            var ragistrations = db.Ragistrations.Include(r => r.BatchDetail).Include(r => r.CourseDetail);
            return View(ragistrations.ToList());
        }

        // GET: Ragistrations/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ragistration ragistration = db.Ragistrations.Find(id);
            if (ragistration == null)
            {
                return HttpNotFound();
            }
            return View(ragistration);
        }

        // GET: Ragistrations/Create
        public ActionResult Create()
        {
            ViewBag.Batch1 = new SelectList(db.BatchDetails, "Id", "Batch");
            ViewBag.Course1 = new SelectList(db.CourseDetails, "Id", "Course");
            return View();
        }

        // POST: Ragistrations/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Firstname,Lastname,Course1,Batch1,DOB,Semester,DOJ,Mbl")] Ragistration ragistration)
        {
            if (ModelState.IsValid)
            {
                db.Ragistrations.Add(ragistration);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Batch1 = new SelectList(db.BatchDetails, "Id", "Batch", ragistration.Batch1);
            ViewBag.Course1 = new SelectList(db.CourseDetails, "Id", "Course", ragistration.Course1);
            return View(ragistration);
        }

        // GET: Ragistrations/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ragistration ragistration = db.Ragistrations.Find(id);
            if (ragistration == null)
            {
                return HttpNotFound();
            }
            ViewBag.Batch1 = new SelectList(db.BatchDetails, "Id", "Batch", ragistration.Batch1);
            ViewBag.Course1 = new SelectList(db.CourseDetails, "Id", "Course", ragistration.Course1);
            return View(ragistration);
        }

        // POST: Ragistrations/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Firstname,Lastname,Course1,Batch1,DOB,Semester,DOJ,Mbl")] Ragistration ragistration)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ragistration).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Batch1 = new SelectList(db.BatchDetails, "Id", "Batch", ragistration.Batch1);
            ViewBag.Course1 = new SelectList(db.CourseDetails, "Id", "Course", ragistration.Course1);
            return View(ragistration);
        }

        // GET: Ragistrations/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ragistration ragistration = db.Ragistrations.Find(id);
            if (ragistration == null)
            {
                return HttpNotFound();
            }
            return View(ragistration);
        }

        // POST: Ragistrations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Ragistration ragistration = db.Ragistrations.Find(id);
            db.Ragistrations.Remove(ragistration);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
