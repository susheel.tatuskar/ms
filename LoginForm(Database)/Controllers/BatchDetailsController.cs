﻿using LoginForm_Database_.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LoginForm_Database_.Controllers
{
    public class BatchDetailsController : Controller
    {
        MVC_Demo1Entities db = new MVC_Demo1Entities();
        // GET: BatchDetails
        public ActionResult Index()
        {
            var data = db.BatchDetails.ToList();
            return View(data);
        }
        public ActionResult Create()
        {

            return View();
        }
        [HttpPost]
        public ActionResult Create(BatchDetail c)
        {
            if (ModelState.IsValid == true)
            {
                db.BatchDetails.Add(c);
                int a = db.SaveChanges();
                if (a > 0)
                {
                    TempData["createmsg"] = "Data Inserted";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["createmsg"] = "Data not Inserted";
                }
            }

            return View();
        }
        public ActionResult Edit(int id)
        {
            var row = db.BatchDetails.Where(model => model.Id == id).FirstOrDefault();

            return View(row);
        }
        [HttpPost]
        public ActionResult Edit(BatchDetail c)
        {

            if (ModelState.IsValid == true)
            {
                db.Entry(c).State = EntityState.Modified;
                int a = db.SaveChanges();
                if (a > 0)
                {
                    TempData["Editmsg"] = "Data Edited";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["Editmsg"] = "Data not Editted";
                }
            }
            return View();
        }

        public ActionResult Delete(int id)
        {
            if (id > 0)
            {
                var deleterow = db.BatchDetails.Where(model => model.Id == id).FirstOrDefault();
                if (deleterow != null)
                {
                    db.Entry(deleterow).State = EntityState.Deleted;
                    int a = db.SaveChanges();
                    if (a > 0)
                    {
                        TempData["Deletemsg"] = "Data deleted";
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["Deletemsg"] = "Data not deleted";
                    }
                }
            }



            return RedirectToAction("Index", "BatchDetails");
        }
        public ActionResult Details(int id)
        {
            var detailsrow = db.BatchDetails.Where(model => model.Id == id).FirstOrDefault();

            return View(detailsrow);
        }
        
    }
}