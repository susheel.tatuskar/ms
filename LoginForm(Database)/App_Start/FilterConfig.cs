﻿using System.Web;
using System.Web.Mvc;

namespace LoginForm_Database_
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
